FROM anapsix/alpine-java:latest

MAINTAINER Brian Schlining <bschlining@gmail.com>

# Fetch and install tomcat
ENV CATALINA_HOME /opt/tomcat
ENV TOMCAT_MAJOR 8
ENV TOMCAT_VERSION 8.0.32
ENV TOMCAT_TGZ_URL https://www.apache.org/dist/tomcat/tomcat-$TOMCAT_MAJOR/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz

# see https://www.apache.org/dist/tomcat/tomcat-8/KEYS
RUN apk upgrade --update \
  && apk add \
    curl \
    tar \
    ca-certificates \
    bash \
  && set -x \
  && curl \
    --silent \
    --location \
    --retry 3 \
    --cacert /etc/ssl/certs/ca-certificates.crt \
    "$TOMCAT_TGZ_URL" | gunzip | tar x -C /usr/ \
  && mv /usr/apache-tomcat* "$CATALINA_HOME" \
  && rm -rf "$CATALINA_HOME/bin/*.bat" \
  && rm -rf "$CATALINA_HOME/webapps/*"
    
# Configure and cleanup tomcat
ENV JMX false 
ENV JMX_PORT 9004 
ENV JMX_HOSTNAME localhost 
ENV DEBUG_PORT 8000 
ENV PERM 128m 
ENV MAXPERM 256m 
ENV MINMEM 128m 
ENV MAXMEM 512m
ENV PATH $CATALINA_HOME/bin:$PATH

EXPOSE 8080 $JMX_PORT $DEBUG_PORT

CMD ["/opt/tomcat/bin/catalina.sh", "run"]