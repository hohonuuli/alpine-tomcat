# alpine-tomcat

This is a lightweight JDK/tomcat installation on Alpine Linux. Currently, it used tomcat 8 and JDK 8. It is a base image used for other services.

Tomcat is installed at `/opt/tomcat` and exposed through port 8080. 8000 is the current debug port and 9004 is the JMX port.